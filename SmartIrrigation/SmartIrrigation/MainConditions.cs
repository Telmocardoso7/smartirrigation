﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartIrrigation
{
    public class MainConditions
    {
        float temp;
        float temp_min;
        float temp_max;
        int humidity;

        public float Temp { get => temp; set => temp = value; }
        public float Temp_min { get => temp_min; set => temp_min = value; }
        public float Temp_max { get => temp_max; set => temp_max = value; }
        public int Humidity { get => humidity; set => humidity = value; }
    }
}