﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartIrrigation
{
    public class leituras
    {
        int id_leituras;
        string sensor_humidade;
        string sensor_temperatura;
        string sensor_ph;
        string caudal;
        string distrito;
        DateTime data;
        int id_arduino;
        int id_localizacao;
        string mes;
        string ano;
        string parque;
        int is_enable;
        string zona;
        string sensor_uv;

        public int Id_leituras { get => id_leituras; set => id_leituras = value; }
        public string Sensor_humidade { get => sensor_humidade; set => sensor_humidade = value; }
        public string Sensor_temperatura { get => sensor_temperatura; set => sensor_temperatura = value; }
        public string Sensor_ph { get => sensor_ph; set => sensor_ph = value; }
        public string Caudal { get => caudal; set => caudal = value; }
        public string Distrito { get => distrito; set => distrito = value; }
        public string Mes { get => mes; set => mes = value; }
        public string Ano { get => ano; set => ano = value; }
        public string Parque { get => parque; set => parque = value; }
        public DateTime Data { get => data; set => data = value; }
        public int Id_arduino { get => id_arduino; set => id_arduino = value; }
        public int Id_localizacao { get => id_localizacao; set => id_localizacao = value; }
        public int Is_enable { get => is_enable; set => is_enable = value; }
        public string Zona { get => zona; set => zona = value; }
        public string Sensor_uv { get => sensor_uv; set => sensor_uv = value; }

    }
}