﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SmartIrrigation
{
    public partial class Distrito : System.Web.UI.Page
    {
        private MySqlConnection mConn;
        List<leituras> listaleituras = new List<leituras>();

        public string id;

        public string jsonStringAr1 = "";


        public static List<Medias> ListaMedias = new List<Medias>();
        public Medias[] entries = ListaMedias.ToArray();
        protected void Page_Load(object sender, EventArgs e)
        {
            ListaMedias = new List<Medias>();
            jsonStringAr1 = "";
            entries = ListaMedias.ToArray();
            string distrito = "Braga";
            GetDados(distrito);

        }

        public void GetDados(string distrito)
        {
            mConn = new MySqlConnection("server=db4free.net;Port=3306;old guids=true;User ID=basededadosteste;password=12345678;database = basededadosteste;");
            mConn.Open();

            if (mConn.State == ConnectionState.Open)
            {
                string query = "SELECT L.id_leitura, L.humidade,L.temperatura,L.timestamp,L.id_arduino,L.ph,L.caudal, L.uv, LO.distrito, LO.id_localizacao, LO.parque FROM leituras L, arduino A, localizacao LO  where "
                    + "  L.id_arduino = A.id_arduino AND A.id_localizacao = LO.id_localizacao and LO.distrito= '" + distrito + "' ORDER BY id_leitura DESC";
                MySqlCommand command = new MySqlCommand(query, mConn);
                MySqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    leituras let = new leituras();
                    let.Id_leituras = (int)dr["id_leitura"];
                    let.Id_arduino = (int)dr["id_arduino"];
                    let.Data = (DateTime)dr["timestamp"];
                    let.Sensor_temperatura = dr["temperatura"].ToString();
                    let.Sensor_humidade = dr["humidade"].ToString();
                    let.Sensor_ph = dr["ph"].ToString();
                    let.Caudal = dr["caudal"].ToString();
                    let.Distrito = (string)dr["distrito"];
                    let.Id_localizacao = (int)dr["id_localizacao"];
                    let.Parque = (string)dr["parque"];
                    let.Sensor_uv = dr["uv"].ToString();

                    listaleituras.Add(let);

                }

                listaleituras = listaleituras.OrderBy(o => o.Id_arduino).ToList();

                var results = from p in listaleituras
                              where p.Parque == "Jardim de Santa Bárbara"
                              group p by p.Parque;

                foreach (var item in results)
                {
                    Medias M = new Medias();
                    M.TempMedia = GetTemperaturas(id, item.ToList());
                    M.HumMedia = GetHumidade(id, item.ToList());
                    //M.PhMedia = GetPh(id, item.ToList());
                    M.CaudalMedia = GetCaudal(id, item.ToList());
                    M.UVMedia = GetUV(id, item.ToList());
                    M.Nome_Parque = item.Key;

                    ListaMedias.Add(M);

                }


                this.entries = ListaMedias.ToArray();



            }
            mConn.Close();
            jsonStringAr1 = JsonConvert.SerializeObject(this.entries);


        }

        public double GetTemperaturas(string id, List<leituras> listaleituras)
        {
            int i = 0;
            int valor = 0;
            double mediaTemp = 0;
            foreach (var leitura in listaleituras)
            {


                valor += Convert.ToInt32(leitura.Sensor_temperatura);

            }

            mediaTemp = valor / listaleituras.Count();

            return mediaTemp;

            //jsonStringTemp = JsonConvert.SerializeObject(arrayTemperatura);
        }


        public double GetHumidade(string id, List<leituras> listaleituras)
        {

            int i = 0;
            int valor = 0;
            double mediaHum = 0;
            foreach (var leitura in listaleituras)
            {

                i++;
                valor += Convert.ToInt32(leitura.Sensor_humidade);
            }
            mediaHum = valor / listaleituras.Count();
            return mediaHum;
            //jsonStringHum = JsonConvert.SerializeObject(arrayHumidade);
        }

        public double GetPh(string id, List<leituras> listaleituras)
        {

            int i = 0;
            int valor = 0;
            double mediaPh = 0;
            foreach (var leitura in listaleituras)
            {

                i++;
                valor += Convert.ToInt32(leitura.Sensor_ph);
            }
            mediaPh = valor / listaleituras.Count();

            return mediaPh;
            //jsonStringPh = JsonConvert.SerializeObject(arrayPh);
        }

        public double GetCaudal(string id, List<leituras> listaleituras)
        {


            int i = 0;
            int valor = 0;
            double mediaCaudal = 0;
            foreach (var leitura in listaleituras)
            {

                i++;
                valor += Convert.ToInt32(leitura.Caudal);
            }
            mediaCaudal = valor / listaleituras.Count();
            return mediaCaudal;
            //jsonStringCaudal = JsonConvert.SerializeObject(arrayCaudal);
        }

        public double GetUV(string id, List<leituras> listaleituras)
        {

            int i = 0;
            double valor = 0;
            double mediaUV = 0;
            foreach (var leitura in listaleituras)
            {

                i++;
                valor += Convert.ToDouble(leitura.Sensor_uv);
            }
            mediaUV = valor / listaleituras.Count();

            return mediaUV;
            //jsonStringUV = JsonConvert.SerializeObject(arrayUV);
        }
    }
}