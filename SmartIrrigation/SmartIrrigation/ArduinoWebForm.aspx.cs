﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using unirest_net.http;

namespace SmartIrrigation
{
    public partial class ArduinoWebForm : System.Web.UI.Page
    {
        private MySqlConnection mConn;
        List<leituras> listaleituras = new List<leituras>();
        public string id;
        public object[,] arrayTemperatura5 = new object[5, 3];
        public object[,] arrayHumidade5 = new object[5, 3];
        public object[,] arrayPh5 = new object[5, 3];
        public object[,] arrayCaudal5 = new object[5, 3];
        public object[,] arrayUV5 = new object[5, 3];
        public object[,] arrayTemperaturaM = new object[12, 3];
        public object[,] arrayHumidadeM = new object[12, 3];
        public object[,] arrayPhM = new object[12, 3];
        public object[,] arrayCaudalM = new object[12, 3];
        public object[,] arrayUVM = new object[12, 3];
        public object[,] arrayTemperaturaA = new object[3, 3];
        public object[,] arrayHumidadeA = new object[3, 3];
        public object[,] arrayPhA = new object[3, 3];
        public object[,] arrayCaudalA = new object[3, 3];
        public object[,] arrayUVA = new object[3, 3];
        public string jsonStringTemp = "";
        public string jsonStringHum = "";
        public string jsonStringPh = "";
        public string jsonStringCaudal = "";
        public string jsonStringUV = "";
        public string filterSelected;
        public string xTitle;
        public string isEnbl;
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Request.QueryString["id"];
            mConn = new MySqlConnection("server=db4free.net;Port=3306;old guids=true;User ID=basededadosteste;password=12345678;database = basededadosteste;");
            var automaticSystem = "False";

            if (IsPostBack)
            {
                return;
            }
            else
            {
                filterSelected = "1";
                xTitle = "Date";
                listaleituras.Clear();
                mConn.Open();

                if (mConn.State == ConnectionState.Open)
                {
                    string query = "SELECT automaticSystem FROM `sistema`";
                    MySqlCommand command = new MySqlCommand(query, mConn);
                    MySqlDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        automaticSystem = dr["automaticSystem"].ToString();
                    }
                }
                mConn.Close();
                if (automaticSystem == "True")
                {
                    Button4.Visible = false;
                }
                else
                {
                    GetIsEnable();
                    if (isEnbl == "True")
                    {
                        Button4.Text = "Power Off Sprinklers";
                        Button4.BackColor = Color.Red;
                    }
                }

                GetDados(id);
                GetTemperaturas(id, listaleituras);
                GetHumidade(id, listaleituras);
                //GetPh(id, listaleituras);
                GetCaudal(id, listaleituras);
                GetUV(id, listaleituras);
            }

        }

        public void reloadPage()
        {
            switch (filterSelected)
            {
                case "1":
                    listaleituras.Clear();
                    GetDados(id);
                    GetTemperaturas(id, listaleituras);
                    GetHumidade(id, listaleituras);
                    //GetPh(id, listaleituras);
                    GetCaudal(id, listaleituras);
                    GetUV(id, listaleituras);
                    break;
                case "2":
                    listaleituras.Clear();
                    GetDadosMensal(id);
                    GetTemperaturas(id, listaleituras);
                    GetHumidade(id, listaleituras);
                    //GetPh(id, listaleituras);
                    GetCaudal(id, listaleituras);
                    GetUV(id, listaleituras);
                    break;
                case "3":
                    listaleituras.Clear();
                    GetDadosAnual(id);
                    GetTemperaturas(id, listaleituras);
                    GetHumidade(id, listaleituras);
                    //GetPh(id, listaleituras);
                    GetCaudal(id, listaleituras);
                    GetUV(id, listaleituras);
                    break;
                default:
                    listaleituras.Clear();
                    GetDados(id);
                    GetTemperaturas(id, listaleituras);
                    GetHumidade(id, listaleituras);
                    //GetPh(id, listaleituras);
                    GetCaudal(id, listaleituras);
                    GetUV(id, listaleituras);
                    break;
            }
        }

        public void GetIsEnable()
        {
            mConn = new MySqlConnection("server=db4free.net;Port=3306;old guids=true;User ID=basededadosteste;password=12345678;database = basededadosteste;");
            mConn.Open();

            if (mConn.State == ConnectionState.Open)
            {
                string query = "SELECT is_enable FROM `arduino` WHERE id_arduino= " + id;
                MySqlCommand command = new MySqlCommand(query, mConn);
                MySqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    isEnbl = dr["is_enable"].ToString();
                }
            }
            mConn.Close();

        }

        public void GetDados(string id)
        {
            mConn = new MySqlConnection("server=db4free.net;Port=3306;old guids=true;User ID=basededadosteste;password=12345678;database = basededadosteste;");
            mConn.Open();
            if (mConn.State == ConnectionState.Open)
            {
                string query = "SELECT L.id_leitura, L.humidade,L.temperatura,L.timestamp,L.id_arduino,L.ph,L.caudal, L.uv, LO.distrito FROM leituras L, arduino A, localizacao LO  where L.id_arduino =" + id
                    + " AND L.id_arduino = A.id_arduino AND A.id_localizacao = LO.id_localizacao ORDER BY timestamp DESC LIMIT 5";
                MySqlCommand command = new MySqlCommand(query, mConn);
                MySqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    leituras let = new leituras();
                    let.Id_leituras = (int)dr["id_leitura"];
                    let.Id_arduino = (int)dr["id_arduino"];
                    let.Data = (DateTime)dr["timestamp"];
                    let.Sensor_temperatura = dr["temperatura"].ToString();
                    let.Sensor_humidade = dr["humidade"].ToString();
                    let.Sensor_ph = dr["ph"].ToString();
                    let.Caudal = dr["caudal"].ToString();
                    let.Distrito = (string)dr["distrito"];
                    let.Sensor_uv = dr["uv"].ToString().Replace(',','.');

                    listaleituras.Add(let);
                }
                listaleituras = listaleituras.OrderBy(o => o.Data).ToList();
            }
            mConn.Close();
        }

        public void GetDadosMensal(string id)
        {
            mConn = new MySqlConnection("server=db4free.net;Port=3306;old guids=true;User ID=basededadosteste;password=12345678;database = basededadosteste;");
            mConn.Open();
            if (mConn.State == ConnectionState.Open)
            {
                string query = "SELECT AVG(humidade) as humidade, AVG(temperatura) as temperatura, AVG(ph) as ph, AVG(caudal) as caudal, AVG(uv) as uv, MONTH(timestamp) as mes FROM leituras  where id_arduino = " + id + " and YEAR(timestamp)= year(CURDATE()) GROUP BY MONTH(timestamp)";
                MySqlCommand command = new MySqlCommand(query, mConn);
                MySqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    leituras let = new leituras();
                    let.Sensor_temperatura = dr["temperatura"].ToString();
                    let.Sensor_humidade = dr["humidade"].ToString();
                    let.Sensor_ph = dr["ph"].ToString();
                    let.Caudal = dr["caudal"].ToString();
                    let.Mes = dr["mes"].ToString();
                    let.Sensor_uv = dr["uv"].ToString().Replace(',', '.');

                    listaleituras.Add(let);
                }
                listaleituras = listaleituras.OrderBy(o => o.Mes).ToList();
            }
            mConn.Close();
        }

        public void GetDadosAnual(string id)
        {
            mConn = new MySqlConnection("server=db4free.net;Port=3306;old guids=true;User ID=basededadosteste;password=12345678;database = basededadosteste;");
            mConn.Open();
            if (mConn.State == ConnectionState.Open)
            {
                string query = "SELECT AVG(humidade) as humidade, AVG(temperatura) as temperatura, AVG(ph) as ph, AVG(caudal) as caudal, AVG(uv) as uv, YEAR(timestamp) as ano FROM leituras where id_arduino = " + id + " AND (Year(timestamp)=Year(CURRENT_TIMESTAMP) OR Year(timestamp)=(Year(CURRENT_TIMESTAMP)-1) OR Year(timestamp)=(Year(CURRENT_TIMESTAMP)-2)) GROUP BY YEAR(timestamp)";
                MySqlCommand command = new MySqlCommand(query, mConn);
                MySqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    leituras let = new leituras();
                    let.Sensor_temperatura = dr["temperatura"].ToString();
                    let.Sensor_humidade = dr["humidade"].ToString();
                    let.Sensor_ph = dr["ph"].ToString();
                    let.Caudal = dr["caudal"].ToString();
                    let.Ano = dr["ano"].ToString();
                    let.Sensor_uv = dr["uv"].ToString().Replace(',', '.');

                    listaleituras.Add(let);
                }
                listaleituras = listaleituras.OrderBy(o => o.Ano).ToList();
            }
            mConn.Close();
        }

        public void GetTemperaturas(string id, List<leituras> listaleituras)
        {
            int i = 0;
            foreach (var leitura in listaleituras)
            {
                switch (filterSelected)
                {
                    case "1":
                        arrayTemperatura5[i, 0] = leitura.Data.Day.ToString() + '/' + leitura.Data.Month.ToString() + "/" + leitura.Data.Year.ToString();
                        arrayTemperatura5[i, 1] = leitura.Sensor_temperatura;
                        arrayTemperatura5[i, 2] = leitura.Sensor_temperatura;
                        i++;
                        break;
                    case "2":
                        CultureInfo Ci = new CultureInfo("en-US");
                        arrayTemperaturaM[i, 0] = Ci.DateTimeFormat.GetMonthName(Convert.ToInt32(leitura.Mes));
                        arrayTemperaturaM[i, 1] = leitura.Sensor_temperatura;
                        arrayTemperaturaM[i, 2] = leitura.Sensor_temperatura;
                        i++;
                        break;
                    case "3":
                        arrayTemperaturaA[i, 0] = leitura.Ano.ToString();
                        arrayTemperaturaA[i, 1] = leitura.Sensor_temperatura;
                        arrayTemperaturaA[i, 2] = leitura.Sensor_temperatura;
                        i++;
                        break;
                    default:
                        arrayTemperatura5[i, 0] = leitura.Data.Day.ToString() + '/' + leitura.Data.Month.ToString() + "/" + leitura.Data.Year.ToString();
                        arrayTemperatura5[i, 1] = leitura.Sensor_temperatura;
                        arrayTemperatura5[i, 2] = leitura.Sensor_temperatura;
                        i++;
                        break;

                }

            }
            switch (filterSelected)
            {
                case "1":
                    jsonStringTemp = JsonConvert.SerializeObject(arrayTemperatura5);
                    break;
                case "2":
                    jsonStringTemp = JsonConvert.SerializeObject(arrayTemperaturaM);
                    break;
                case "3":
                    jsonStringTemp = JsonConvert.SerializeObject(arrayTemperaturaA);
                    break;
                default:
                    jsonStringTemp = JsonConvert.SerializeObject(arrayTemperatura5);
                    break;
            }
        }

        public void GetHumidade(string id, List<leituras> listaleituras)
        {
            int i = 0;
            foreach (var leitura in listaleituras)
            {
                switch (filterSelected)
                {
                    case "1":
                        arrayHumidade5[i, 0] = leitura.Data.Day.ToString() + '/' + leitura.Data.Month.ToString() + "/" + leitura.Data.Year.ToString();
                        arrayHumidade5[i, 1] = leitura.Sensor_humidade;
                        arrayHumidade5[i, 2] = leitura.Sensor_humidade;
                        i++;
                        break;
                    case "2":
                        CultureInfo Ci = new CultureInfo("en-US");
                        arrayHumidadeM[i, 0] = Ci.DateTimeFormat.GetMonthName(Convert.ToInt32(leitura.Mes));
                        arrayHumidadeM[i, 1] = leitura.Sensor_humidade;
                        arrayHumidadeM[i, 2] = leitura.Sensor_humidade;
                        i++;
                        break;
                    case "3":
                        arrayHumidadeA[i, 0] = leitura.Ano.ToString();
                        arrayHumidadeA[i, 1] = leitura.Sensor_humidade;
                        arrayHumidadeA[i, 2] = leitura.Sensor_humidade;
                        i++;
                        break;
                    default:
                        arrayHumidade5[i, 0] = leitura.Data.Day.ToString() + '/' + leitura.Data.Month.ToString() + "/" + leitura.Data.Year.ToString();
                        arrayHumidade5[i, 1] = leitura.Sensor_humidade;
                        arrayHumidade5[i, 2] = leitura.Sensor_humidade;
                        i++;
                        break;
                }
            }
            switch (filterSelected)
            {
                case "1":
                    jsonStringHum = JsonConvert.SerializeObject(arrayHumidade5);
                    break;
                case "2":
                    jsonStringHum = JsonConvert.SerializeObject(arrayHumidadeM);
                    break;
                case "3":
                    jsonStringHum = JsonConvert.SerializeObject(arrayHumidadeA);
                    break;
                default:
                    jsonStringHum = JsonConvert.SerializeObject(arrayHumidade5);
                    break;
            }
        }

        public void GetPh(string id, List<leituras> listaleituras)
        {
            int i = 0;
            foreach (var leitura in listaleituras)
            {
                switch (filterSelected)
                {
                    case "1":
                        arrayPh5[i, 0] = leitura.Data.Day.ToString() + '/' + leitura.Data.Month.ToString() + "/" + leitura.Data.Year.ToString();
                        arrayPh5[i, 1] = leitura.Sensor_ph;
                        arrayPh5[i, 2] = leitura.Sensor_ph;
                        i++;
                        break;
                    case "2":
                        CultureInfo Ci = new CultureInfo("en-US");
                        arrayPhM[i, 0] = Ci.DateTimeFormat.GetMonthName(Convert.ToInt32(leitura.Mes));
                        arrayPhM[i, 1] = leitura.Sensor_ph;
                        arrayPhM[i, 2] = leitura.Sensor_ph;
                        i++;
                        break;
                    case "3":
                        arrayPhA[i, 0] = leitura.Ano.ToString();
                        arrayPhA[i, 1] = leitura.Sensor_ph;
                        arrayPhA[i, 2] = leitura.Sensor_ph;
                        i++;
                        break;
                    default:
                        arrayPh5[i, 0] = leitura.Data.Day.ToString() + '/' + leitura.Data.Month.ToString() + "/" + leitura.Data.Year.ToString();
                        arrayPh5[i, 1] = leitura.Sensor_ph;
                        arrayPh5[i, 2] = leitura.Sensor_ph;
                        i++;
                        break;
                }

            }
            switch (filterSelected)
            {
                case "1":
                    jsonStringPh = JsonConvert.SerializeObject(arrayPh5);
                    break;
                case "2":
                    jsonStringPh = JsonConvert.SerializeObject(arrayPhM);
                    break;
                case "3":
                    jsonStringPh = JsonConvert.SerializeObject(arrayPhA);
                    break;
                default:
                    jsonStringPh = JsonConvert.SerializeObject(arrayPh5);
                    break;
            }
        }

        public void GetCaudal(string id, List<leituras> listaleituras)
        {
            int i = 0;
            foreach (var leitura in listaleituras)
            {
                switch (filterSelected)
                {
                    case "1":
                        arrayCaudal5[i, 0] = leitura.Data.Day.ToString() + '/' + leitura.Data.Month.ToString() + "/" + leitura.Data.Year.ToString();
                        arrayCaudal5[i, 1] = leitura.Caudal;
                        arrayCaudal5[i, 2] = leitura.Caudal;
                        i++;
                        break;
                    case "2":
                        CultureInfo Ci = new CultureInfo("en-US");
                        arrayCaudalM[i, 0] = Ci.DateTimeFormat.GetMonthName(Convert.ToInt32(leitura.Mes));
                        arrayCaudalM[i, 1] = leitura.Caudal;
                        arrayCaudalM[i, 2] = leitura.Caudal;
                        i++;
                        break;
                    case "3":
                        arrayCaudalA[i, 0] = leitura.Ano.ToString();
                        arrayCaudalA[i, 1] = leitura.Caudal;
                        arrayCaudalA[i, 2] = leitura.Caudal;
                        i++;
                        break;
                    default:
                        arrayCaudal5[i, 0] = leitura.Data.Day.ToString() + '/' + leitura.Data.Month.ToString() + "/" + leitura.Data.Year.ToString();
                        arrayCaudal5[i, 1] = leitura.Caudal;
                        arrayCaudal5[i, 2] = leitura.Caudal;
                        i++;
                        break;
                }
            }
            switch (filterSelected)
            {
                case "1":
                    jsonStringCaudal = JsonConvert.SerializeObject(arrayCaudal5);
                    break;
                case "2":
                    jsonStringCaudal = JsonConvert.SerializeObject(arrayCaudalM);
                    break;
                case "3":
                    jsonStringCaudal = JsonConvert.SerializeObject(arrayCaudalA);
                    break;
                default:
                    jsonStringCaudal = JsonConvert.SerializeObject(arrayCaudal5);
                    break;
            }
        }
        public void GetUV(string id, List<leituras> listaleituras)
        {
            int i = 0;
            foreach (var leitura in listaleituras)
            {
                switch (filterSelected)
                {
                    case "1":
                        arrayUV5[i, 0] = leitura.Data.Day.ToString() + '/' + leitura.Data.Month.ToString() + "/" + leitura.Data.Year.ToString();
                        arrayUV5[i, 1] = leitura.Sensor_uv;
                        arrayUV5[i, 2] = leitura.Sensor_uv;
                        i++;
                        break;
                    case "2":
                        CultureInfo Ci = new CultureInfo("en-US");
                        arrayUVM[i, 0] = Ci.DateTimeFormat.GetMonthName(Convert.ToInt32(leitura.Mes));
                        arrayUVM[i, 1] = leitura.Sensor_uv;
                        arrayUVM[i, 2] = leitura.Sensor_uv;
                        i++;
                        break;
                    case "3":
                        arrayUVA[i, 0] = leitura.Ano.ToString();
                        arrayUVA[i, 1] = leitura.Sensor_uv;
                        arrayUVA[i, 2] = leitura.Sensor_uv;
                        i++;
                        break;
                    default:
                        arrayUV5[i, 0] = leitura.Data.Day.ToString() + '/' + leitura.Data.Month.ToString() + "/" + leitura.Data.Year.ToString();
                        arrayUV5[i, 1] = leitura.Sensor_uv;
                        arrayUV5[i, 2] = leitura.Sensor_uv;
                        i++;
                        break;
                }

            }
            switch (filterSelected)
            {
                case "1":
                    jsonStringUV = JsonConvert.SerializeObject(arrayUV5);
                    break;
                case "2":
                    jsonStringUV = JsonConvert.SerializeObject(arrayUVM);
                    break;
                case "3":
                    jsonStringUV = JsonConvert.SerializeObject(arrayUVA);
                    break;
                default:
                    jsonStringUV = JsonConvert.SerializeObject(arrayUV5);
                    break;
            }
        }
        protected void btn1_Click(object sender, EventArgs e)
        {
            filterSelected = "1";
            xTitle = "Date";
            reloadPage();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            filterSelected = "2";
            xTitle = "Month";
            reloadPage();
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            filterSelected = "3";
            xTitle = "Year";
            reloadPage();
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            GetIsEnable();

            mConn = new MySqlConnection("server=db4free.net;Port=3306;old guids=true;User ID=basededadosteste;password=12345678;database = basededadosteste;");
            mConn.Open();

            if (mConn.State == ConnectionState.Open)
            {
                if (isEnbl == "True")
                {
                    string queryUpdate = "UPDATE `arduino` SET `is_enable`= 0  WHERE id_arduino= " + id;
                    MySqlCommand commandUpdate = new MySqlCommand(queryUpdate, mConn);
                    commandUpdate.ExecuteNonQuery();
                    isEnbl = "False";
                    Button4.Text = "Activate Sprinklers";
                    Button4.BackColor = Color.Green;
                    reloadPage();
                }
                else
                {
                    string queryUpdate = "UPDATE `arduino` SET `is_enable`= 1  WHERE id_arduino= " + id;
                    MySqlCommand commandUpdate = new MySqlCommand(queryUpdate, mConn);
                    commandUpdate.ExecuteNonQuery();
                    isEnbl = "True";
                    Button4.Text = "Power Off Sprinklers";
                    Button4.BackColor = Color.Red;
                    reloadPage();
                }
            }
            mConn.Close();
        }
    }
}