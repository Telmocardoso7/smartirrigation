﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartIrrigation
{
    public class forecast
    {
        Day day;
        Day night;

        public Day Day { get => day; set => day = value; }
        public Day Night { get => night; set => night = value; }
    }
}