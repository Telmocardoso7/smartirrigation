﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartIrrigation
{
    public class alertSystem
    {

        int id_arduino;
        string sensor;
        int outlier_num;
        string zona;
        string parque;


        public int Id_arduino { get => id_arduino; set => id_arduino = value; }
        public string Sensor { get => sensor; set => sensor = value; }
        public int Outlier_num { get => outlier_num; set => outlier_num = value; }
        public string Zona { get => zona; set => zona = value; }
        public string Parque { get => parque; set => parque = value; }

    }
}