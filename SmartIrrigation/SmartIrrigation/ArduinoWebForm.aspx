﻿<%@ Page Title="Arduino Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ArduinoWebForm.aspx.cs" Inherits="SmartIrrigation.ArduinoWebForm" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">



    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <link href="Content/Site.css" rel="stylesheet" />
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <div align="center">
        <br />
        <label>Filters: </label>
        <asp:Button ID="btn1" class="mybtn" runat="server" Text="Last 5 readings" OnClick="btn1_Click" />
        <asp:Button ID="Button2" class="mybtn" runat="server" Text="Monthly" OnClick="Button2_Click" />
        <asp:Button ID="Button3" class="mybtn" runat="server" Text="Yearly" OnClick="Button3_Click" />
        <div align="right">
            <asp:Button ID="Button4" class="mybtn" runat="server" Text="Activate Sprinklers" OnClick="Button4_Click" />
        </div>
    </div>
    <br />
    <br />
    <br />
    <div class="jumbotron" style="margin-left: -100px; width: 1350px; margin-top: -50px;">
        <div id="linechart_material" style="width: 100%; height: 500px;"></div>
        <br />
        <div id="linechart_materia" style="width: 100%; height: 500px;"></div>
        <br />
        <div id="linechart_uv" style="width: 100%; height: 500px;"></div>
        <%--<div id="linechart_ph" style="width: 100%; height: 500px;"></div>--%>
        <br />
        <div id="linechart_caudal" style="width: 100%; height: 500px;"></div>
    </div>
    <script>
        google.charts.load('current', { 'packages': ['corechart'] });
        google.charts.setOnLoadCallback(drawChart);
        google.charts.setOnLoadCallback(drawChartHum);
        google.charts.setOnLoadCallback(drawChartUV);
        google.charts.setOnLoadCallback(drawChartCaudal);

        function drawChart() {
            var data = new google.visualization.DataTable();
            var xTitle = ('<%=xTitle%>');
            data.addColumn('string', xTitle);
            data.addColumn('number', 'Temperature (ºC)');

            data.addColumn({ type: 'number', role: 'annotation' });
            data.addColumn('number', '<=8ºC');
            data.addColumn('number', '8ºC-15ºC')
            data.addColumn('number', '15ºC-26ºC');
            data.addColumn('number', '26ºC-32ºC');
            data.addColumn('number', '>=32ºC');
            var array = JSON.parse('<%=jsonStringTemp%>');

            for (var i = 0; i < array.length; i++) {
                var array2 = [];

                for (j = 0; j < array[i].length; j++) {
                    if (typeof array[i][j] === 'string') {
                        if (j !== 0) {
                            array[i][j] = parseInt(array[i][j], 10)
                        }
                    }
                    array2.push(array[i][j])



                }
                array2.push(8)
                array2.push(7)
                array2.push(11)
                array2.push(6)
                array2.push(18)
                console.log(array2)
                data.addRow(array2)

            }


            var options = {
                title: 'Temperature (ºC)',
                hAxis: { title: xTitle, titleTextStyle: { color: '#333' } },
                vAxis: { minValue: 0, maxValue: 50 },
                series: {
                    0: {

                        type: 'line',
                        color: '#696969'
                    },
                    1: {
                        lineWidth: 0,
                        type: 'area',
                        visibleInLegend: true,
                        enableInteractivity: false,
                        // example of setting the color of an area
                        color: 'red',
                        pointSize: 0


                    },
                    2: {
                        lineWidth: 0,
                        type: 'area',
                        visibleInLegend: true,
                        enableInteractivity: false,
                        color: 'yellow',
                        pointSize: 0
                    },
                    3: {
                        lineWidth: 0,
                        type: 'area',
                        visibleInLegend: true,
                        enableInteractivity: false,
                        color: 'green',
                        pointSize: 0
                    },
                    4: {
                        lineWidth: 0,
                        type: 'area',
                        visibleInLegend: true,
                        enableInteractivity: false,
                        color: 'yellow',
                        pointSize: 0
                    },
                    5: {
                        lineWidth: 0,
                        type: 'area',
                        visibleInLegend: true,
                        enableInteractivity: false,
                        color: 'red',
                        pointSize: 0
                    },
                },
                pointSize: 6,
                isStacked: true

            };

            ;

            var chart = new google.visualization.AreaChart(document.getElementById('linechart_material'));
            chart.draw(data, options);
        }

        function drawChartHum() {
            var data = new google.visualization.DataTable();
            var xTitle = ('<%=xTitle%>');
            data.addColumn('string', xTitle);
            data.addColumn('number', 'Moisture (%)');

            data.addColumn({ type: 'number', role: 'annotation' });
            data.addColumn('number', '<=40');
            data.addColumn('number', '40-50');
            data.addColumn('number', '50-60');
            data.addColumn('number', '60-80');
            data.addColumn('number', '>=80');
            var array = JSON.parse('<%=jsonStringHum%>');
            //console.log(array);



            for (var i = 0; i < array.length; i++) {
                var array2 = [];
                for (j = 0; j < array[i].length; j++) {
                    if (typeof array[i][j] === 'string') {
                        if (j !== 0) {
                            array[i][j] = parseInt(array[i][j], 10)
                        }
                    }
                    array2.push(array[i][j])
                }
                array2.push(40)
                array2.push(10)
                array2.push(10)
                array2.push(20)
                array2.push(20)
                data.addRow(array2)

            }

            //console.log(data)
            var options = {
                title: 'Moisture (%)',
                hAxis: { title: xTitle, titleTextStyle: { color: '#333' } },
                vAxis: { minValue: 0, maxValue: 100 },
                series: {
                    0: {

                        type: 'line',
                        color: '#696969'
                    },
                    1: {
                        lineWidth: 0,
                        type: 'area',
                        visibleInLegend: true,
                        enableInteractivity: false,
                        // example of setting the color of an area
                        color: 'red',
                        pointSize: 0


                    },
                    2: {
                        lineWidth: 0,
                        type: 'area',
                        visibleInLegend: true,
                        enableInteractivity: false,
                        color: 'yellow',
                        pointSize: 0
                    },
                    3: {
                        lineWidth: 0,
                        type: 'area',
                        visibleInLegend: true,
                        enableInteractivity: false,
                        color: 'green',
                        pointSize: 0
                    },
                    4: {
                        lineWidth: 0,
                        type: 'area',
                        visibleInLegend: true,
                        enableInteractivity: false,
                        color: 'yellow',
                        pointSize: 0
                    },
                    5: {
                        lineWidth: 0,
                        type: 'area',
                        visibleInLegend: true,
                        enableInteractivity: false,
                        color: 'red',
                        pointSize: 0
                    },
                },
                pointSize: 6,
                isStacked: true

            }

            var chart = new google.visualization.LineChart(document.getElementById('linechart_materia'));
            chart.draw(data, options);
        }

        function drawChartPh() {
            var data = new google.visualization.DataTable();
            var xTitle = ('<%=xTitle%>');
            data.addColumn('string', xTitle);
            data.addColumn('number', 'PH');
            data.addColumn({ type: 'number', role: 'annotation' });
            data.addColumn('number', '<5');
            data.addColumn('number', '5-5,5');
            data.addColumn('number', '5,5-6,5');
            data.addColumn('number', '6,5-7,5');
            data.addColumn('number', '>=7,5');
            var array = JSON.parse('<%=jsonStringPh%>');
            //console.log(array);



            for (var i = 0; i < array.length; i++) {
                var array2 = [];
                for (j = 0; j < array[i].length; j++) {
                    if (typeof array[i][j] === 'string') {
                        if (j !== 0) {
                            array[i][j] = parseInt(array[i][j], 10)
                        }
                    }
                    array2.push(array[i][j])
                }
                array2.push(5)
                array2.push(0.5)
                array2.push(1)
                array2.push(1)
                array2.push(6.5)
                data.addRow(array2)
            }

            //console.log(data)
            var options = {
                title: 'PH',
                hAxis: { title: xTitle, titleTextStyle: { color: '#333' } },
                vAxis: { minValue: 0, maxValue: 14 },
                series: {
                    0: {

                        type: 'line',
                        color: '#696969'
                    },
                    1: {
                        lineWidth: 0,
                        type: 'area',
                        visibleInLegend: true,
                        enableInteractivity: false,
                        // example of setting the color of an area
                        color: 'red',
                        pointSize: 0


                    },
                    2: {
                        lineWidth: 0,
                        type: 'area',
                        visibleInLegend: true,
                        enableInteractivity: false,
                        color: 'yellow',
                        pointSize: 0
                    },
                    3: {
                        lineWidth: 0,
                        type: 'area',
                        visibleInLegend: true,
                        enableInteractivity: false,
                        color: 'green',
                        pointSize: 0
                    },
                    4: {
                        lineWidth: 0,
                        type: 'area',
                        visibleInLegend: true,
                        enableInteractivity: false,
                        color: 'yellow',
                        pointSize: 0
                    },
                    5: {
                        lineWidth: 0,
                        type: 'area',
                        visibleInLegend: true,
                        enableInteractivity: false,
                        color: 'red',
                        pointSize: 0
                    }
                },

                pointSize: 6,
                isStacked: true

            }

            var chart = new google.visualization.LineChart(document.getElementById('linechart_ph'));
            chart.draw(data, options);
        }


        function drawChartCaudal() {
            var data = new google.visualization.DataTable();
            var xTitle = ('<%=xTitle%>');
            data.addColumn('string', xTitle);
            data.addColumn('number', 'Flow (l/m)');

            data.addColumn({ type: 'number', role: 'annotation' });
            data.addColumn('number', '<=15 l/m');
            data.addColumn('number', '15-20 l/m');
            data.addColumn('number', '>=20 l/m');
            var array = JSON.parse('<%=jsonStringCaudal%>');
            //console.log(array);



            for (var i = 0; i < array.length; i++) {
                var array2 = [];
                for (j = 0; j < array[i].length; j++) {
                    if (typeof array[i][j] === 'string') {
                        if (j !== 0) {
                            array[i][j] = parseInt(array[i][j], 10)
                        }
                    }

                    array2.push(array[i][j])
                }
                array2.push(15)
                array2.push(5)
                array2.push(10)
                data.addRow(array2)
            }

            //console.log(data)
            var options = {
                title: 'Flow (l/m)',
                hAxis: { title: xTitle, titleTextStyle: { color: '#333' } },
                vAxis: { minValue: 0, maxValue: 30 },
                series: {
                    0: {

                        type: 'line',
                        color: '#696969'
                    },
                    1: {
                        lineWidth: 0,
                        type: 'area',
                        visibleInLegend: true,
                        enableInteractivity: false,
                        // example of setting the color of an area
                        color: 'green',
                        pointSize: 0


                    },
                    2: {
                        lineWidth: 0,
                        type: 'area',
                        visibleInLegend: true,
                        enableInteractivity: false,
                        color: 'yellow',
                        pointSize: 0
                    },
                    3: {
                        lineWidth: 0,
                        type: 'area',
                        visibleInLegend: true,
                        enableInteractivity: false,
                        color: 'red',
                        pointSize: 0
                    },

                },
                pointSize: 6,
                isStacked: true

            }

            var chart = new google.visualization.LineChart(document.getElementById('linechart_caudal'));
            chart.draw(data, options);
        }

        function drawChartUV() {
            var data = new google.visualization.DataTable();
            var xTitle = ('<%=xTitle%>');
            data.addColumn('string', xTitle);
            data.addColumn('number', 'UV');
            data.addColumn({ type: 'number', role: 'annotation' });
            data.addColumn('number', '<3');
            data.addColumn('number', '3-6');
            data.addColumn('number', '6-8');
            data.addColumn('number', '8-11');
            data.addColumn('number', '>=11');
            var array = JSON.parse('<%=jsonStringUV%>');
            //console.log(array);



            for (var i = 0; i < array.length; i++) {
                var array2 = [];
                for (j = 0; j < array[i].length; j++) {
                    if (typeof array[i][j] === 'string') {
                        if (j !== 0) {
                            array[i][j] = parseFloat(array[i][j])
                        }
                    }
                    array2.push(array[i][j])
                }
                array2.push(5)
                array2.push(0.5)
                array2.push(1)
                array2.push(1)
                array2.push(6.5)
                data.addRow(array2)
            }

            //console.log(data)
            var options = {
                title: 'UV',
                hAxis: { title: xTitle, titleTextStyle: { color: '#333' } },
                vAxis: { minValue: 0, maxValue: 14 },
                series: {
                    0: {

                        type: 'line',
                        color: '#696969'
                    },
                    1: {
                        lineWidth: 0,
                        type: 'area',
                        visibleInLegend: true,
                        enableInteractivity: false,
                        // example of setting the color of an area
                        color: 'green',
                        pointSize: 0


                    },
                    2: {
                        lineWidth: 0,
                        type: 'area',
                        visibleInLegend: true,
                        enableInteractivity: false,
                        color: 'yellow',
                        pointSize: 0
                    },
                    3: {
                        lineWidth: 0,
                        type: 'area',
                        visibleInLegend: true,
                        enableInteractivity: false,
                        color: 'orange',
                        pointSize: 0
                    },
                    4: {
                        lineWidth: 0,
                        type: 'area',
                        visibleInLegend: true,
                        enableInteractivity: false,
                        color: 'red',
                        pointSize: 0
                    },
                    5: {
                        lineWidth: 0,
                        type: 'area',
                        visibleInLegend: true,
                        enableInteractivity: false,
                        color: 'red',
                        pointSize: 0
                    }
                },

                pointSize: 6,
                isStacked: true

            }

            var chart = new google.visualization.LineChart(document.getElementById('linechart_uv'));
            chart.draw(data, options);
        }
    //$(document).ready(function() {
    //    reloadupdate();
    //    setInterval(function(){
    //        reloadupdate()
    //    }, 25000);
    //    // ajusta a regua
    //    setInterval(function(){
    //      var comprimento = $('circle')[5].getAttribute('cx');
    //      $('.regua').css('width', comprimento);
    //    }, 1500)
    //});
    </script>
</asp:Content>

