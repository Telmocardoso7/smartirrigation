﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Distrito.aspx.cs" Inherits="SmartIrrigation.Distrito" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Distritos</title>
    <style>
        body{
            padding-top:20px;
        }
        .canvasjs-chart-credit{
            color:white !important;
        }
    </style>
    <script>
        window.onload = function () {
            ShowHumidade()
            //ShowPh()
            ShowUV()
            ShowCaudal()
            var dps = []; //dataPoints. 
            var array = JSON.parse('<%=jsonStringAr1%>');
            addDataPointsAndRender(array)
                   
                       
                   var chart = new CanvasJS.Chart("chartContainer", {
                       
	            animationEnabled: true,
	            theme: "light2", // "light1", "light2", "dark1", "dark2"
	            title:{
		            text: "Temperature of the district of Braga by park"
	            },
	            axisY: {
		            title: "Temperature (ºC)"
	            },
	            data: [{        
		            type: "column",  
		            showInLegend: true, 
		            legendMarkerColor: "grey",
                    legendText: "Park",
                    dataPoints:dps
		            //dataPoints: [      
			           // { y: 300878, label: "Venezuela" },
			           // { y: 266455,  label: "Saudi" },
			           // { y: 169709,  label: "Canada" },
			           // { y: 158400,  label: "Iran" },
			           // { y: 142503,  label: "Iraq" },
			           // { y: 101500, label: "Kuwait" },
			           // { y: 97800,  label: "UAE" },
			           // { y: 80000,  label: "Russia" }
		            //]
	            }]
                   });
                   function addDataPointsAndRender(array) {
                       
                       for (var i = 0; i < array.length; i++) {
                           dps.push({
                           label: array[i].Nome_Parque,
                           y: array[i].TempMedia
                           });
                           
                       }
                    
                       
            }
            chart.render();
        }

        function ShowHumidade() {
            var dps = []; //dataPoints. 
            var array = JSON.parse('<%=jsonStringAr1%>');
            addDataPointsAndRender(array)
                   
                   var chart = new CanvasJS.Chart("chartContainer2", {
                       
	            animationEnabled: true,
	            theme: "light2", // "light1", "light2", "dark1", "dark2"
	            title:{
		            text: "Moisture  of the district of Braga by park"
	            },
	            axisY: {
		            title: "Moisture (%)"
	            },
	            data: [{        
		            type: "column",  
		            showInLegend: true, 
		            legendMarkerColor: "grey",
                    legendText: "ParK",
                    dataPoints:dps
		            
	            }]
                   });
                   function addDataPointsAndRender(array) {
                       
                       for (var i = 0; i < array.length; i++) {
                           dps.push({
                           label: array[i].Nome_Parque,
                           y: array[i].HumMedia
                           });
                           
                       }
                      
                       
            }
            chart.render();
        }

        function ShowPh() {
            var dps = []; //dataPoints. 
            var array = JSON.parse('<%=jsonStringAr1%>');
            addDataPointsAndRender(array)
                   
                      
                   var chart = new CanvasJS.Chart("chartContainer3", {
                       
	            animationEnabled: true,
	            theme: "light2", // "light1", "light2", "dark1", "dark2"
	            title:{
		            text: "PH of the district of Braga by park"
	            },
	            axisY: {
		            title: "PH"
	            },
	            data: [{        
		            type: "column",  
		            showInLegend: true, 
		            legendMarkerColor: "grey",
                    legendText: "Park",
                    dataPoints:dps
		            
	            }]
                   });
                   function addDataPointsAndRender(array) {
                       
                       for (var i = 0; i < array.length; i++) {
                           dps.push({
                           label: array[i].Nome_Parque,
                           y: array[i].PhMedia
                           });
                           
                       }
                      
                       
            }
            chart.render();
        }
        function ShowUV() {
            var dps = []; //dataPoints. 
            var array = JSON.parse('<%=jsonStringAr1%>');
            addDataPointsAndRender(array)
                   
                      
                   var chart = new CanvasJS.Chart("chartContainer3", {
                       
	            animationEnabled: true,
	            theme: "light2", // "light1", "light2", "dark1", "dark2"
	            title:{
		            text: "UV of the district of Braga by park"
	            },
	            axisY: {
		            title: "UV"
	            },
	            data: [{        
		            type: "column",  
		            showInLegend: true, 
		            legendMarkerColor: "grey",
                    legendText: "Park",
                    dataPoints:dps
		            
	            }]
                   });
                   function addDataPointsAndRender(array) {
                       
                       for (var i = 0; i < array.length; i++) {
                           dps.push({
                           label: array[i].Nome_Parque,
                           y: array[i].UVMedia
                           });
                           
                       }
                      
                       
            }
            chart.render();
        }
        function ShowCaudal() {
            var dps = []; //dataPoints. 
            var array = JSON.parse('<%=jsonStringAr1%>');
            addDataPointsAndRender(array)
                   
                       console.log(dps)
                   var chart = new CanvasJS.Chart("chartContainer4", {
                       
	            animationEnabled: true,
	            theme: "light2", // "light1", "light2", "dark1", "dark2"
	            title:{
		            text: "Flow of the district of Braga by park"
	            },
	            axisY: {
		            title: "Flow"
	            },
	            data: [{        
		            type: "column",  
		            showInLegend: true, 
		            legendMarkerColor: "grey",
                    legendText: "Park",
                    dataPoints:dps
		            
	            }]
                   });
                   function addDataPointsAndRender(array) {
                       
                       for (var i = 0; i < array.length; i++) {
                           dps.push({
                           label: array[i].Nome_Parque,
                           y: array[i].CaudalMedia
                           });
                           
                       }
                       console.log(dps)
                       
            }
            chart.render();
}

    </script>
    </head>
  <body>
    <div id="chartContainer" style="height: 370px; width: 100%;"></div>
      <br />
      <br />
      <br />
      <br />
    <div id="chartContainer2" style="height: 370px; width: 100%;"></div>
      <br />
      <br />
      <br />
      <br />
    <div id="chartContainer3" style="height: 370px; width: 100%;"></div>
      <br />
      <br />
      <br />
      <br />
    <div id="chartContainer4" style="height: 370px; width: 100%;"></div>

    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</body>
</html>
