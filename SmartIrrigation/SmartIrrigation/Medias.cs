﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartIrrigation
{
    public class Medias
    {
        public double TempMedia { get; set; }
        public double HumMedia { get; set; }
        public double PhMedia { get; set; }
        public double CaudalMedia { get; set; }
        public double UVMedia { get; set; }
        public string Nome_Parque { get; set; }
    }
}