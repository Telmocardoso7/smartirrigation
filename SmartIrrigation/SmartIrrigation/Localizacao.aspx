﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Localizacao.aspx.cs" Inherits="SmartIrrigation.Localizacao" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ZingSoft Demo</title>
    <style>
        html,
        body {
            height: 100%;
            width: 100%;
        }

        #myChart {
            height: 100%;
            width: 100%;
            min-height: 250px;
        }

        #myChartPh {
            height: 100%;
            width: 100%;
            min-height: 250px;
        }

        #myChartUV {
            height: 100%;
            width: 100%;
            min-height: 250px;
        }

        #myChartHum {
            height: 100%;
            width: 100%;
            min-height: 250px;
        }

        #myChartCaudal {
            height: 100%;
            width: 100%;
            min-height: 250px;
        }

        .zc-ref {
            display: none;
            height: 100%;
            width: 100%;
            border: 5px solid green;
        }

        zing-grid[loading] {
            height: 100%;
        }

        #myChart-wrapper {
            left: 25%;
            top: 0px;
        }

        #myChartHum-wrapper {
            left: 25%;
            top: 0px;
        }

        #myChartPh-wrapper {
            left: 25%;
            top: 0px;
        }

        #myChartUV-wrapper {
            left: 25%;
            top: 0px;
        }

        #myChartCaudal-wrapper {
            left: 25%;
            top: 0px;
        }

        .title {
            color: red;
            left: 150px;
            position: relative;
            top: 100px;
            text-decoration: underline;
        }
    </style>
    <script src="https://cdn.zingchart.com/zingchart.min.js"></script>
    <link href="Content/Site.css" rel="stylesheet" />
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>
<body>





    <div id='myChart'><a class="zc-ref" href="https://www.zingchart.com/">Powered by ZingChart</a></div>



    <div id='myChartHum'><a class="zc-ref" href="https://www.zingchart.com/">Powered by ZingChart</a></div>



    <%--<div id='myChartPh'><a class="zc-ref" href="https://www.zingchart.com/">Powered by ZingChart</a></div>--%>
    <div id='myChartUV'><a class="zc-ref" href="https://www.zingchart.com/">Powered by ZingChart</a></div>



    <div id='myChartCaudal'><a class="zc-ref" href="https://www.zingchart.com/">Powered by ZingChart</a></div>



    <script>
        ZC.LICENSE = ["b55b025e438fa8a98e32482b5f768ff5"];
        var myConfig = {
            type: 'radar',
            legend: {

            },
            plot: {
                aspect: 'area',
                animation: {
                    effect: 3,
                    sequence: 1,
                    speed: 700
                }
            },
            scaleV: {
                visible: false
            },
            scaleK: {
                values: '0:4:1',
                labels: ['Zone 1', 'Zone 2', 'Zone 3', 'Zone 4', 'Zone 5'],
                item: {
                    fontColor: '#607D8B',
                    backgroundColor: "white",
                    borderColor: "#aeaeae",
                    borderWidth: 1,
                    padding: '5 10',
                    borderRadius: 10
                },
                refLine: {
                    lineColor: '#c10000'
                },
                tick: {
                    lineColor: '#59869c',
                    lineWidth: 2,
                    lineStyle: 'dotted',
                    size: 20
                },
                guide: {
                    lineColor: "#607D8B",
                    lineStyle: 'solid',
                    alpha: 0.3,
                    backgroundColor: "#c5c5c5 #718eb4"
                }
            },
            series: [
                {
                    values: [JSON.parse('<%=arrayArduino1[0]%>'), JSON.parse('<%=arrayArduino2[0]%>'), JSON.parse('<%=arrayArduino3[0]%>'), JSON.parse('<%=arrayArduino4[0]%>'), JSON.parse('<%=arrayArduino5[0]%>')],
                    lineColor: '#53a534',
                    backgroundColor: '#689F38',
                    text: "Temperature (ºC)"

                }
            ]
        };


        var myConfigHum = {
            type: 'radar',
            legend: {

            },
            plot: {
                aspect: 'area',
                animation: {
                    effect: 3,
                    sequence: 1,
                    speed: 700
                }
            },
            scaleV: {
                visible: false
            },
            scaleK: {
                values: '0:4:1',
                labels: ['Zone 1', 'Zone 2', 'Zone 3', 'Zone 4', 'Zone 5'],
                item: {
                    fontColor: '#607D8B',
                    backgroundColor: "white",
                    borderColor: "#aeaeae",
                    borderWidth: 1,
                    padding: '5 10',
                    borderRadius: 10
                },
                refLine: {
                    lineColor: '#c10000'
                },
                tick: {
                    lineColor: '#59869c',
                    lineWidth: 2,
                    lineStyle: 'dotted',
                    size: 20
                },
                guide: {
                    lineColor: "#607D8B",
                    lineStyle: 'solid',
                    alpha: 0.3,
                    backgroundColor: "#c5c5c5 #718eb4"
                }
            },
            series: [
                {
                    values: [JSON.parse('<%=arrayArduino1[1]%>'), JSON.parse('<%=arrayArduino2[1]%>'), JSON.parse('<%=arrayArduino3[1]%>'), JSON.parse('<%=arrayArduino4[1]%>'), JSON.parse('<%=arrayArduino5[1]%>')],
                    lineColor: 'yellow',
                    backgroundColor: 'yellow',
                    text: "Moisture (%)"


                }
            ]
        };


<%--        var myConfigPh = {
            type: 'radar',
            legend: {

            },
            plot: {
                aspect: 'area',
                animation: {
                    effect: 3,
                    sequence: 1,
                    speed: 700
                }
            },
            scaleV: {
                visible: false
            },
            scaleK: {
                values: '0:4:1',
                labels: ['Zone 1', 'Zone 2', 'Zone 3', 'Zone 4', 'Zone 5'],
                item: {
                    fontColor: '#607D8B',
                    backgroundColor: "white",
                    borderColor: "#aeaeae",
                    borderWidth: 1,
                    padding: '5 10',
                    borderRadius: 10
                },
                refLine: {
                    lineColor: '#c10000'
                },
                tick: {
                    lineColor: '#59869c',
                    lineWidth: 2,
                    lineStyle: 'dotted',
                    size: 20
                },
                guide: {
                    lineColor: "#607D8B",
                    lineStyle: 'solid',
                    alpha: 0.3,
                    backgroundColor: "#c5c5c5 #718eb4"
                }
            },
            series: [
                {
                    values: [JSON.parse('<%=arrayArduino1[2]%>'), JSON.parse('<%=arrayArduino2[2]%>'), JSON.parse('<%=arrayArduino3[2]%>'), JSON.parse('<%=arrayArduino4[2]%>'), JSON.parse('<%=arrayArduino5[2]%>')],
                    lineColor: '#00008B',
                    backgroundColor: '#00008B',
                    text: "PH"


                }
            ]
        };--%>
        var myConfigUV = {
            type: 'radar',
            legend: {

            },
            plot: {
                aspect: 'area',
                animation: {
                    effect: 3,
                    sequence: 1,
                    speed: 700
                }
            },
            scaleV: {
                visible: false
            },
            scaleK: {
                values: '0:4:1',
                labels: ['Zone 1', 'Zone 2', 'Zone 3', 'Zone 4', 'Zone 5'],
                item: {
                    fontColor: '#607D8B',
                    backgroundColor: "white",
                    borderColor: "#aeaeae",
                    borderWidth: 1,
                    padding: '5 10',
                    borderRadius: 10
                },
                refLine: {
                    lineColor: '#c10000'
                },
                tick: {
                    lineColor: '#59869c',
                    lineWidth: 2,
                    lineStyle: 'dotted',
                    size: 20
                },
                guide: {
                    lineColor: "#607D8B",
                    lineStyle: 'solid',
                    alpha: 0.3,
                    backgroundColor: "#c5c5c5 #718eb4"
                }
            },
            series: [
                {
                    values: [JSON.parse('<%=arrayArduino1[2]%>'), JSON.parse('<%=arrayArduino2[2]%>'), JSON.parse('<%=arrayArduino3[2]%>'), JSON.parse('<%=arrayArduino4[2]%>'), JSON.parse('<%=arrayArduino5[2]%>')],
                    lineColor: '#00008B',
                    backgroundColor: '#00008B',
                    text: "UV"


                }
            ]
        };

        var myConfigCaudal = {
            type: 'radar',
            legend: {

            },
            plot: {
                aspect: 'area',
                animation: {
                    effect: 3,
                    sequence: 1,
                    speed: 700
                }
            },
            scaleV: {
                visible: false
            },
            scaleK: {
                values: '0:4:1',
                labels: ['Zone 1', 'Zone 2', 'Zone 3', 'Zone 4', 'Zone 5'],
                item: {
                    fontColor: '#607D8B',
                    backgroundColor: "white",
                    borderColor: "#aeaeae",
                    borderWidth: 1,
                    padding: '5 10',
                    borderRadius: 10
                },
                refLine: {
                    lineColor: '#c10000'
                },
                tick: {
                    lineColor: '#59869c',
                    lineWidth: 2,
                    lineStyle: 'dotted',
                    size: 20
                },
                guide: {
                    lineColor: "#607D8B",
                    lineStyle: 'solid',
                    alpha: 0.3,
                    backgroundColor: "#c5c5c5 #718eb4"
                }
            },
            series: [
                {
                    values: [JSON.parse('<%=arrayArduino1[3]%>'), JSON.parse('<%=arrayArduino2[3]%>'), JSON.parse('<%=arrayArduino3[3]%>'), JSON.parse('<%=arrayArduino4[3]%>'), JSON.parse('<%=arrayArduino5[3]%>')],
                    lineColor: '#DC143C',
                    backgroundColor: '#DC143C',
                    text: "Flow (l/m)"


                }
            ]
        };

        zingchart.render({
            id: 'myChart',
            data: myConfig,
            height: '700px',
            width: '700px',
            top: '0px',
            padding: '0px'

        });

        zingchart.render({
            id: 'myChartHum',
            data: myConfigHum,
            height: '700px',
            width: '700px',
            top: '0px',
            padding: '0px'

        });

        //zingchart.render({
        //    id: 'myChartPh',
        //    data: myConfigPh,
        //    height: '700px',
        //    width: '700px',
        //    top: '0px',
        //    padding: '0px'

        //});

        zingchart.render({
            id: 'myChartUV',
            data: myConfigUV,
            height: '700px',
            width: '700px',
            top: '0px',
            padding: '0px'

        });

        zingchart.render({
            id: 'myChartCaudal',
            data: myConfigCaudal,
            height: '700px',
            width: '700px',
            top: '0px',
            padding: '0px'

        });
    </script>

</body>
</html>
