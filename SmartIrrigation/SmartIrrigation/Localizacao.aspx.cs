﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SmartIrrigation
{
    public partial class Localizacao : System.Web.UI.Page
    {

        private MySqlConnection mConn;
        List<leituras> listaleituras = new List<leituras>();
        public string parque;
        public double[] arrayArduino1 = new double[4];
        public double[] arrayArduino2 = new double[4];
        public double[] arrayArduino3 = new double[4];
        public double[] arrayArduino4 = new double[4];
        public double[] arrayArduino5 = new double[4];
        public string jsonStringAr1 = "";
        public string jsonStringAr2 = "";
        public string jsonStringAr3 = "";
        public string jsonStringAr4 = "";
        public string jsonStringAr5 = "";
        public int Id = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            parque = Request.QueryString["parque"];
            //id = "1";
            GetDados(parque);
        }

        public void GetDados(string parque)
        {
            mConn = new MySqlConnection("server=db4free.net;Port=3306;old guids=true;User ID=basededadosteste;password=12345678;database = basededadosteste;");
            mConn.Open();
            double TempMedia = 0;
            double HumMedia = 0;
            double CaudalMedia = 0;
            //double PhMedia = 0;
            double UVMedia = 0;

            int i = 0;

            if (mConn.State == ConnectionState.Open)
            {
                string query = "SELECT L.id_leitura, L.humidade,L.temperatura,L.timestamp,L.id_arduino,L.ph,L.caudal, L.uv, LO.distrito, LO.id_localizacao FROM leituras L, arduino A, localizacao LO  " +
                    "where L.id_arduino = A.id_arduino AND A.id_localizacao = LO.id_localizacao AND LO.parque = \""+ parque +"\" ORDER BY id_leitura DESC";
                MySqlCommand command = new MySqlCommand(query, mConn);
                MySqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    leituras let = new leituras();
                    let.Id_leituras = (int)dr["id_leitura"];
                    let.Id_arduino = (int)dr["id_arduino"];
                    let.Data = (DateTime)dr["timestamp"];
                    let.Sensor_temperatura = dr["temperatura"].ToString();
                    let.Sensor_humidade = dr["humidade"].ToString();
                    let.Sensor_ph = dr["ph"].ToString();
                    let.Caudal = dr["caudal"].ToString();
                    let.Distrito = (string)dr["distrito"];
                    let.Id_localizacao = (int)dr["id_localizacao"];
                    let.Sensor_uv = dr["uv"].ToString();

                    listaleituras.Add(let);
                    
                }

                listaleituras = listaleituras.OrderBy(o => o.Id_arduino).ToList();
                var results = from p in listaleituras
                              group p by p.Id_arduino;

                foreach (var item in results)
                {
                    TempMedia = GetTemperaturas(item.ToList());
                    HumMedia = GetHumidade(item.ToList());
                    //PhMedia = GetPh(item.ToList());
                    CaudalMedia = GetCaudal(item.ToList());
                    UVMedia = GetUV(item.ToList());

                    double[] arrayAuxiliar = new double[4];
                    if (this.arrayArduino1[0]==0)
                    {
                        this.arrayArduino1[0] = TempMedia;
                        this.arrayArduino1[1] = HumMedia;
                        //this.arrayArduino1[2] = PhMedia;
                        this.arrayArduino1[2] = UVMedia;
                        this.arrayArduino1[3] = CaudalMedia;

                    }
                    else
                    {
                        if (this.arrayArduino2[0] == 0)
                        {
                            this.arrayArduino2[0] = TempMedia;
                            this.arrayArduino2[1] = HumMedia;
                            //this.arrayArduino2[2] = PhMedia;
                            this.arrayArduino2[2] = UVMedia;
                            this.arrayArduino2[3] = CaudalMedia;

                        }
                        else
                        {
                            if (this.arrayArduino3[0] == 0)
                            {
                                this.arrayArduino3[0] = TempMedia;
                                this.arrayArduino3[1] = HumMedia;
                                //this.arrayArduino3[2] = PhMedia;
                                this.arrayArduino3[2] = UVMedia;
                                this.arrayArduino3[3] = CaudalMedia;

                            }
                            else
                            {
                                if (this.arrayArduino4[0] == 0)
                                {
                                    this.arrayArduino4[0] = TempMedia;
                                    this.arrayArduino4[1] = HumMedia;
                                    //this.arrayArduino4[2] = PhMedia;
                                    this.arrayArduino4[2] = UVMedia;
                                    this.arrayArduino4[3] = CaudalMedia;

                                }
                                else
                                {
                                    if (this.arrayArduino5[0] == 0)
                                    {
                                        this.arrayArduino5[0] = TempMedia;
                                        this.arrayArduino5[1] = HumMedia;
                                        //this.arrayArduino5[2] = PhMedia;
                                        this.arrayArduino5[2] = UVMedia;
                                        this.arrayArduino5[3] = CaudalMedia;

                                    }

                                }
                            }
                        }
                    }
                    



                }


            }
            mConn.Close();
            jsonStringAr1 = JsonConvert.SerializeObject(this.arrayArduino1);
            jsonStringAr2 = JsonConvert.SerializeObject(this.arrayArduino2);
            jsonStringAr3 = JsonConvert.SerializeObject(this.arrayArduino3);
            jsonStringAr4 = JsonConvert.SerializeObject(this.arrayArduino4);
            jsonStringAr4 = JsonConvert.SerializeObject(this.arrayArduino5);

        }

        public double GetTemperaturas(List<leituras> listaleituras)
        {
            int valor = 0;
            double mediaTemp = 0;
            foreach (var leitura in listaleituras)
            {
                valor += Convert.ToInt32(leitura.Sensor_temperatura);
            }

            mediaTemp = valor / listaleituras.Count();

            return mediaTemp;

            //jsonStringTemp = JsonConvert.SerializeObject(arrayTemperatura);
        }

       
        public double GetHumidade(List<leituras> listaleituras)
        {
            int valor = 0;
            double mediaHum = 0;
            foreach (var leitura in listaleituras)
            {
                valor += Convert.ToInt32(leitura.Sensor_humidade);
            }
            mediaHum = valor / listaleituras.Count();
            return mediaHum;
            //jsonStringHum = JsonConvert.SerializeObject(arrayHumidade);
        }

        public double GetPh(List<leituras> listaleituras)
        {
            int valor = 0;
            double mediaPh = 0;
            foreach (var leitura in listaleituras)
            {
                valor += Convert.ToInt32(leitura.Sensor_ph);
            }
            mediaPh = valor / listaleituras.Count();

            return mediaPh;
            //jsonStringPh = JsonConvert.SerializeObject(arrayPh);
        }

        public double GetCaudal(List<leituras> listaleituras)
        {
            int valor = 0;
            double mediaCaudal = 0;
            foreach (var leitura in listaleituras)
            {
                valor += Convert.ToInt32(leitura.Caudal);
            }
            mediaCaudal = valor / listaleituras.Count();
            return mediaCaudal;
            //jsonStringCaudal = JsonConvert.SerializeObject(arrayCaudal);
        }

        public double GetUV(List<leituras> listaleituras)
        {

            double valor = 0;
            double mediaUV = 0;
            foreach (var leitura in listaleituras)
            {
                valor += Convert.ToDouble(leitura.Sensor_uv);
            }
            mediaUV = valor / listaleituras.Count();

            return Math.Round(mediaUV);
            //jsonStringUV = JsonConvert.SerializeObject(arrayUV);
        }
    }
}