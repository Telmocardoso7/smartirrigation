﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SmartIrrigation._Default" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Content/Site.css" rel="stylesheet" />
    <img src="Content/Index.png" style="width: 1350px; margin-left: -100px; margin-top: -10px; height: 500px;" />
    <div class="jumbotron" style="margin-left: -100px; width: 1350px; margin-top: -50px;">
        <div align="center">
            <br />
            <asp:Button ID="btn1" class="mybtn" runat="server" Text="Activate Automatic Irrigation System" OnClick="Button1_Click" />
        </div>
        <br />
        <div align="center">
            <h2>Arduinos's Location</h2>
            <p class="lead">In the map below it is possible to observe the location of the different arduinos.</p>
        </div>
        <p>
            <div align="center">
                <iframe src="https://www.google.com/maps/d/embed?mid=1CmEEcRHVQUUEuiMXrwO3VmauoFRA9Cvv&hl=pt-PT" width="1000" height="600"></iframe>
                <%--<iframe src="https://www.google.com/maps/d/embed?mid=1H--Fv47MD_Kp0UBmPqHBmaFq8FBMEmyW" width="1000" height="600"></iframe>--%>
            </div>
        </p>
    </div>
    <div align="center">
        <h1>Arduinos</h1>
    </div>
    <br />
    <div class="container">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="thumbnail img-thumb-bg" style="background-image: url('Content/JardimDeSantaBarbaraFoto.jpg'); background-size: 400px; border-radius: 25px; padding: 50px">
                            <div class="overlay"></div>
                            <div class="caption">
                                <div class="tag"><a href="ArduinoWebForm.aspx?id=1">Santa Barbara Garden</a></div>
                                <div class="title"><a href="ArduinoWebForm.aspx?id=1">Zone 1</a></div>
                                <div class="content">
                                    <p>Arduino 1 implemented in Santa Barbara Garden</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="thumbnail img-thumb-bg" style="background-image: url('Content/JardimDeSantaBarbaraFoto.jpg'); background-size: 400px; border-radius: 25px; padding: 50px">
                            <div class="overlay"></div>
                            <div class="caption">
                                <div class="tag"><a href="ArduinoWebForm.aspx?id=2">Santa Barbara Garden</a></div>
                                <div class="title"><a href="ArduinoWebForm.aspx?id=2">Zone 2</a></div>
                                <div class="content">
                                    <p>Arduino 2 implemented in Santa Barbara Garden</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="thumbnail img-thumb-bg" style="background-image: url('Content/JardimDeSantaBarbaraFoto.jpg'); background-size: 400px; border-radius: 25px; padding: 50px">
                            <div class="overlay"></div>
                            <div class="caption">
                                <div class="tag"><a href="ArduinoWebForm.aspx?id=3">Santa Barbara Garden</a></div>
                                <div class="title"><a href="ArduinoWebForm.aspx?id=3">Zone 3</a></div>
                                <div class="content">
                                    <p>Arduino 3 implemented in Santa Barbara Garden</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="thumbnail img-thumb-bg" style="background-image: url('Content/JardimDeSantaBarbaraFoto.jpg'); background-size: 400px; border-radius: 25px; padding: 50px">
                            <div class="overlay"></div>
                            <div class="caption">
                                <div class="tag"><a href="ArduinoWebForm.aspx?id=4">Santa Barbara Garden</a></div>
                                <div class="title"><a href="ArduinoWebForm.aspx?id=4">Zone 4</a></div>
                                <div class="content">
                                    <p>Arduino 4 implemented in Santa Barbara Garden</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="thumbnail img-thumb-bg" style="background-image: url('Content/JardimDeSantaBarbaraFoto.jpg'); background-size: 400px; border-radius: 25px; padding: 50px">
                            <div class="overlay"></div>
                            <div class="caption">
                                <div class="tag"><a href="ArduinoWebForm.aspx?id=5">Santa Barbara Garden</a></div>
                                <div class="title"><a href="ArduinoWebForm.aspx?id=5">Zone 5</a></div>
                                <div class="content">
                                    <p>Arduino 5 implemented in Santa Barbara Garden</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr />
                <hr />
                <%--          <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail img-thumb-bg" style="background-image: url('Content/JardimDaAvenidaCentral.jpg'); background-size: 400px; border-radius: 25px;padding: 50px  ">
                        <div class="overlay"></div>
                        <div class="caption">
                            <div class="tag"><a href="ArduinoWebForm.aspx?id=6">Avenida Central Garden</a></div>
                            <div class="title"><a href="ArduinoWebForm.aspx?id=6">Zone 1</a></div>
                            <div class="content">
                                <p>Arduino 1 implemented in Avenida Central Garden</p>
                            </div>
                        </div>
                    </div>
                </div>
                   <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail img-thumb-bg" style="background-image: url('Content/JardimDaAvenidaCentral.jpg'); background-size: 400px; border-radius: 25px;padding: 50px  ">
                        <div class="overlay"></div>
                        <div class="caption">
                            <div class="tag"><a href="ArduinoWebForm.aspx?id=7">Avenida Central Garden</a></div>
                            <div class="title"><a href="ArduinoWebForm.aspx?id=7">Zone 2</a></div>
                            <div class="content">
                                <p>Arduino 2 implemented in Avenida Central Garden</p>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail img-thumb-bg" style="background-image: url('Content/JardimDaAvenidaCentral.jpg'); background-size: 400px; border-radius: 25px;padding: 50px  ">
                        <div class="overlay"></div>
                        <div class="caption">
                            <div class="tag"><a href="ArduinoWebForm.aspx?id=8">Avenida Central Garden</a></div>
                            <div class="title"><a href="ArduinoWebForm.aspx?id=8">Zone 3</a></div>
                            <div class="content">
                                <p>Arduino 3 implemented in Avenida Central Garden</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail img-thumb-bg" style="background-image: url('Content/JardimDaAvenidaCentral.jpg'); background-size: 400px; border-radius: 25px;padding: 50px  ">
                        <div class="overlay"></div>
                        <div class="caption">
                            <div class="tag"><a href="ArduinoWebForm.aspx?id=9">Avenida Central Garden</a></div>
                            <div class="title"><a href="ArduinoWebForm.aspx?id=9">Zone 4</a></div>
                            <div class="content">
                                <p>Arduino 4 implemented in Avenida Central Garden</p>
                            </div>
                        </div>
                    </div>
                </div>
                   <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail img-thumb-bg" style="background-image: url('Content/JardimDaAvenidaCentral.jpg'); background-size: 400px; border-radius: 25px;padding: 50px  ">
                        <div class="overlay"></div>
                        <div class="caption">
                            <div class="tag"><a href="ArduinoWebForm.aspx?id=10">Avenida Central Garden</a></div>
                            <div class="title"><a href="ArduinoWebForm.aspx?id=10">Zone 5</a></div>
                            <div class="content">
                                <p>Arduino 5 implemented in Avenida Central Garden</p>
                            </div>
                        </div>
                    </div>
                </div>
              </div>--%>
            </div>
        </div>
    </div>
    <br />
    <div align="center">
        <h1>Location</h1>
    </div>
    <br />
    <div class="container">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="thumbnail img-thumb-bg" style="background-image: url('Content/JardimDeSantaBarbaraFoto.jpg'); background-size: 400px; border-radius: 25px; padding: 50px">
                            <div class="overlay"></div>
                            <div class="caption">
                                <div class="tag"><a href="Localizacao.aspx?parque=Jardim de Santa Bárbara">Santa Barbara Garden</a></div>
                                <div class="title"><a href="Localizacao.aspx?parque=Jardim de Santa Bárbara">Arduinos in Santa Barbara Garden</a></div>
                                <div class="content">
                                    <p>All data about arduinos in Santa Barbara Garden</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--             <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail img-thumb-bg" style="background-image: url('Content/JardimDaAvenidaCentral.jpg'); background-size: 400px; border-radius: 25px;padding: 50px  ">
                        <div class="overlay"></div>
                        <div class="caption">
                            <div class="tag"><a href="Localizacao.aspx?parque=Jardim Da Avenida Central">Avenida Central Garden</a></div>
                            <div class="title"><a href="Localizacao.aspx?parque=Jardim Da Avenida Central">Arduinos in Avenida Central Garden</a></div>
                            <div class="content">
                                <p>All data about arduinos in Avenida Central Garden</p>
                            </div>
                        </div>
                    </div>
                </div>--%>
                </div>
            </div>
        </div>
    </div>
    <br />
    <div align="center">
        <h1>District</h1>
    </div>
    <br />
    <div class="col-lg-4 col-md-4 col-sm-6">
        <div class="thumbnail img-thumb-bg" style="background-image: url('Content/braga.jpg'); background-size: 400px; border-radius: 25px; padding: 50px">
            <div class="overlay"></div>
            <div class="caption">
                <div class="tag"><a href="Distrito.aspx?distrito=Braga">Braga</a></div>
                <div class="title"><a href="Distrito.aspx?distrito=Braga">Arduinos in Braga</a></div>
                <div class="content">
                    <p>Data referring to all arduinos of Braga.</p>
                </div>
            </div>
        </div>
    </div>
    <br />
    <br />
    <p>
        &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
        <br />
        <br />

    </p>
</asp:Content>
