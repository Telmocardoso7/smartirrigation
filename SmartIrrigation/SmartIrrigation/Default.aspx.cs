﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SmartIrrigation
{
    public partial class _Default : Page
    {
        private MySqlConnection mConn;
        List<leituras> listaleituras = new List<leituras>();
        List<alertSystem> aS = new List<alertSystem>();
        private readonly object balanceLock = new object();
        private readonly object locker = new object();
        private bool allDisable = true;
        private bool rainForecast = false;//ver que valor este tem de ter inicialmente
        private static string automaticSystem = "False"; //para sabermos se estamos a tentar ativar ou desativar o sistema de rega automatico
        private Task task1;
        private bool GetCurrentConditions = false; //Activate SA API call to save current consitions

        protected void Page_Load(object sender, EventArgs e)
        {
            if (GetCurrentConditions)
            {
                GetAPICurrentConditions();
            }
            if (!IsPostBack)
            {
                mConn = new MySqlConnection("server=db4free.net;Port=3306;old guids=true;User ID=basededadosteste;password=12345678;database = basededadosteste;");
                mConn.Open();

                if (mConn.State == ConnectionState.Open)
                {
                    string query = "UPDATE `sistema` SET `automaticSystem`= 0";//sistema automático desativado
                    MySqlCommand command = new MySqlCommand(query, mConn);
                    command.ExecuteNonQuery();
                }
                mConn.Close();
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            var ts = new CancellationTokenSource();
            CancellationToken ct = ts.Token;
            mConn = new MySqlConnection("server=db4free.net;Port=3306;old guids=true;User ID=basededadosteste;password=12345678;database = basededadosteste;");

            mConn.Open();

            if (mConn.State == ConnectionState.Open)
            {
                string query = "SELECT automaticSystem FROM `sistema`";
                MySqlCommand command = new MySqlCommand(query, mConn);
                MySqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    automaticSystem = dr["automaticSystem"].ToString();
                }
            }
            mConn.Close();

            if (automaticSystem == "False")//Se estiver a falso significa que está desativado por isso se o botão foi clicacdo é para ativar
            {

                mConn.Open();

                if (mConn.State == ConnectionState.Open)
                {
                    string query = "UPDATE `arduino` SET `is_enable`= 0"; //são desativados todos os sensores
                    MySqlCommand command = new MySqlCommand(query, mConn);
                    command.ExecuteNonQuery();
                }
                mConn.Close();

                task1 = Task.Factory.StartNew(() => GetTemperaturasAndSmartSystem(), ct); //e iniciada uma thread que vai ficar a executar esta função ate que o botão seja novamente carregado -
                                                                                          //para conseguir dar f11 nesta função a unica forma e colocando um breakpoint dentro deste método
                btn1.Text = "Disable Automatic Irrigation System";
                btn1.BackColor = Color.Red;

                mConn.Open();

                if (mConn.State == ConnectionState.Open)
                {
                    string query = "UPDATE `sistema` SET `automaticSystem`= 1";//sistema automático ativado
                    MySqlCommand command = new MySqlCommand(query, mConn);
                    command.ExecuteNonQuery();
                }
                mConn.Close();

            }
            else //significa que queremos desativar o sistema automatico de reha
            {
                btn1.Text = "Activate Automatic Irrigation System";
                btn1.BackColor = Color.Green;

                mConn.Open();

                if (mConn.State == ConnectionState.Open)
                {
                    string query = "UPDATE `sistema` SET `automaticSystem`= 0";
                    MySqlCommand command = new MySqlCommand(query, mConn);
                    command.ExecuteNonQuery();
                }
                mConn.Close();
            }
        }

        public int PositionInAlertSystem(List<leituras> aux, string sensor)
        {
            int position = 0;
            foreach (var aux_aS in aS)
            {
                if (aux_aS.Id_arduino == aux[0].Id_arduino && aux_aS.Sensor == sensor)
                {
                    return position;
                }
                position++;
            }
            return -1;
        }
        public List<int> PositionInAlertSystemToSendEmail()
        {
            List<int> pos = new List<int>();
            int position = 0;
            foreach (var aux_aS in aS)
            {
                if (aux_aS.Outlier_num >= 1)//alterar
                {
                    pos.Add(position);
                }
                position++;
            }
            return pos;
        }
        public void SendEmail(List<int> index)
        {
            try
            {
                var body = "<p>An outliers sequence was detected at the following location:</p>";
                foreach (var aux in index)
                {
                    body += "<p> Park: " + aS[aux].Parque + " | Zone: " + aS[aux].Zona + " | Sensor: " + aS[aux].Sensor + " </p>";
                }
                body += "<p> Please verify this situations as soon as possible</p>";
                var message = new MailMessage();
                message.To.Add(new MailAddress("telmo.cardoso.6@hotmail.com"));  // replace with valid value 
                message.From = new MailAddress("alertsystemum@gmail.com");  // replace with valid value
                message.Subject = "Alert System";
                message.Body = string.Format(body, "AlertSystemUM", "alertsystemum@gmail.com");
                message.IsBodyHtml = true;

                using (var smtp = new SmtpClient("smtp.gmail.com"))
                {
                    var credential = new NetworkCredential
                    {
                        UserName = "alertsystemum@gmail.com",  // replace with valid value
                        Password = "Aaaaaaa1$"  // replace with valid value
                    };
                    smtp.Credentials = credential;
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.Send(message);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        public void GetTemperaturasAndSmartSystem()
        {
            lock (balanceLock)
            {

                string urlLocation = "http://dataservice.accuweather.com/locations/v1/cities/search?apikey=YYNuNGQLhYlBuVZw4iTXaBKJzGJbCJ5T&q=Braga%2CPortugal";
                var client = new WebClient();
                string content = client.DownloadString(urlLocation);
                var locationKey = JsonConvert.DeserializeObject<List<location>>(content);

                string urlForecast = "http://dataservice.accuweather.com/forecasts/v1/daily/5day/" + locationKey[0].Key.ToString() + "?apikey=%09YYNuNGQLhYlBuVZw4iTXaBKJzGJbCJ5T";
                var client1 = new WebClient();
                string content1 = client1.DownloadString(urlForecast);
                content1 = content1.Substring(content1.IndexOf('['));
                content1 = content1.Remove(content1.Length - 1);
                var forecast5days = JsonConvert.DeserializeObject<List<forecast>>(content1);


                while (true)
                {
                    if (automaticSystem == "True")
                    {
                        // another thread decided to cancel
                        Console.WriteLine("task canceled");
                        break;
                    }
                    if (mConn.State == ConnectionState.Open) {
                        mConn.Close();
                    }
                    mConn.Open();
                    if (mConn.State == ConnectionState.Open)
                    {
                        string query = "SELECT P.id_arduino,P.id_leitura,P.humidade,P.temperatura,P.timestamp,P.ph,P.caudal, P.uv, P.distrito, P.is_enable, P.parque, P.zona " +
                            "FROM (SELECT L.id_leitura, L.humidade,L.temperatura,L.timestamp,L.id_arduino,L.ph,L.caudal, L.uv, LO.distrito, A.is_enable, LO.parque, LO.zona FROM leituras L, arduino A, localizacao LO where" +
                            " L.id_arduino = A.id_arduino AND A.id_localizacao = LO.id_localizacao ORDER BY L.timestamp DESC) P";
                        MySqlCommand command = new MySqlCommand(query, mConn);
                        MySqlDataReader dr = command.ExecuteReader();
                        while (dr.Read())
                        {
                            leituras let = new leituras();
                            let.Id_leituras = (int)dr["id_leitura"];
                            let.Id_arduino = (int)dr["id_arduino"];
                            let.Data = (DateTime)dr["timestamp"];
                            let.Sensor_temperatura = dr["temperatura"].ToString();
                            let.Sensor_humidade = dr["humidade"].ToString();
                            let.Sensor_ph = dr["ph"].ToString();
                            let.Caudal = dr["caudal"].ToString();
                            let.Distrito = (string)dr["distrito"];
                            let.Is_enable = Convert.ToInt32(dr["is_enable"]);
                            let.Parque = (string)dr["parque"];
                            let.Zona = (string)dr["zona"];
                            let.Sensor_uv = dr["uv"].ToString();

                            listaleituras.Add(let);
                        }
                    }
                    mConn.Close();

                    var results = from p in listaleituras
                                  group p by p.Id_arduino;

                    //Alert System

                    foreach (var item in results)
                    {
                        List<leituras> aux = item.ToList();
                        if (Convert.ToInt32(aux[0].Sensor_humidade) > 700 || Convert.ToInt32(aux[0].Sensor_humidade) < 150)
                        {
                            if (PositionInAlertSystem(aux, "Moisture") != -1)
                            {
                                aS[PositionInAlertSystem(aux, "Moisture")].Outlier_num++;
                            }
                            else
                            {
                                alertSystem alS = new alertSystem();
                                alS.Id_arduino = aux[0].Id_arduino;
                                alS.Sensor = "Moisture";
                                alS.Outlier_num = 1;
                                alS.Zona = aux[0].Zona;
                                alS.Parque = aux[0].Parque;
                                aS.Add(alS);
                            }
                        }
                        if (Convert.ToInt32(aux[0].Sensor_temperatura) > 50 || Convert.ToInt32(aux[0].Sensor_temperatura) < -20)
                        {
                            if (PositionInAlertSystem(aux, "Temperature") != -1)
                            {
                                aS[PositionInAlertSystem(aux, "Temperature")].Outlier_num++;
                            }
                            else
                            {
                                alertSystem alS = new alertSystem();
                                alS.Id_arduino = aux[0].Id_arduino;
                                alS.Sensor = "Temperature";
                                alS.Outlier_num = 1;
                                alS.Zona = aux[0].Zona;
                                alS.Parque = aux[0].Parque;
                                aS.Add(alS);
                            }
                        }
                        /*if (Convert.ToInt32(aux[0].Sensor_ph) > 9 || Convert.ToInt32(aux[0].Sensor_ph) < 4)
                        {
                            if (PositionInAlertSystem(aux, "Ph") != -1)
                            {
                                aS[PositionInAlertSystem(aux, "Ph")].Outlier_num++;
                            }
                            else
                            {
                                alertSystem alS = new alertSystem();
                                alS.Id_arduino = aux[0].Id_arduino;
                                alS.Sensor = "Ph";
                                alS.Outlier_num = 1;
                                alS.Zona = aux[0].Zona;
                                alS.Parque = aux[0].Parque;
                                aS.Add(alS);
                            }
                        }*/
                        if (Convert.ToInt32(aux[0].Caudal) > 25)
                        {
                            if (PositionInAlertSystem(aux, "Flow") != -1)
                            {
                                aS[PositionInAlertSystem(aux, "Flow")].Outlier_num++;
                            }
                            else
                            {
                                alertSystem alS = new alertSystem();
                                alS.Id_arduino = aux[0].Id_arduino;
                                alS.Sensor = "Flow";
                                alS.Outlier_num = 1;
                                alS.Zona = aux[0].Zona;
                                alS.Parque = aux[0].Parque;
                                aS.Add(alS);
                            }
                        }
                        if (Convert.ToDouble(aux[0].Sensor_uv) > 8 || Convert.ToDouble(aux[0].Sensor_uv) < 0)
                        {
                            if (PositionInAlertSystem(aux, "UV") != -1)
                            {
                                aS[PositionInAlertSystem(aux, "UV")].Outlier_num++;
                            }
                            else
                            {
                                alertSystem alS = new alertSystem();
                                alS.Id_arduino = aux[0].Id_arduino;
                                alS.Sensor = "UV";
                                alS.Outlier_num = 1;
                                alS.Zona = aux[0].Zona;
                                alS.Parque = aux[0].Parque;
                                aS.Add(alS);
                            }
                        }
                    }

                    //createExcelFile(results);
                    //Smart Irrigation
                    foreach (var item in results)
                    {


                        List<leituras> aux = item.ToList();

                        if (aux[0].Is_enable == 1)
                        {
                            if (Convert.ToInt32(aux[0].Sensor_humidade) > 300)
                            {
                                mConn.Open();

                                if (mConn.State == ConnectionState.Open)
                                {
                                    string query = "UPDATE `arduino` SET `is_enable`= 0 where id_arduino = " + aux[0].Id_arduino;
                                    MySqlCommand command = new MySqlCommand(query, mConn);
                                    command.ExecuteNonQuery();
                                }
                                mConn.Close();
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(aux[0].Sensor_humidade) < 300)
                            {
                                if (forecast5days[1].Day.IconPhrase.ToString() != "Rain" || forecast5days[1].Day.IconPhrase.ToString() != "Showers" || forecast5days[1].Night.IconPhrase.ToString() != "Showers" || forecast5days[1].Night.IconPhrase.ToString() != "Rain")
                                {
                                    if (Convert.ToInt32(aux[0].Sensor_temperatura) > 14 && Convert.ToInt32(aux[0].Sensor_temperatura) < 26)
                                    {
                                        if (DateTime.Now.Hour < 7 || DateTime.Now.Hour > 21)
                                        {
                                            mConn.Open();

                                            if (mConn.State == ConnectionState.Open)
                                            {
                                                string query = "UPDATE `arduino` SET `is_enable`= 1 where id_arduino = " + aux[0].Id_arduino;
                                                MySqlCommand command = new MySqlCommand(query, mConn);
                                                command.ExecuteNonQuery();
                                            }
                                            mConn.Close();
                                            allDisable = false;
                                        }
                                    }
                                }
                                else
                                {
                                    if (Convert.ToInt32(aux[0].Sensor_humidade) < 150)
                                    {
                                        if (DateTime.Now.Hour < 9 || DateTime.Now.Hour > 20)
                                        {
                                            mConn.Open();

                                            if (mConn.State == ConnectionState.Open)
                                            {
                                                string query = "UPDATE `arduino` SET `is_enable`= 1 where id_arduino = " + aux[0].Id_arduino;
                                                MySqlCommand command = new MySqlCommand(query, mConn);
                                                command.ExecuteNonQuery();
                                            }
                                            mConn.Close();
                                            allDisable = false;
                                            rainForecast = true;
                                        }
                                    }
                                }

                            }
                        }


                    }

                    if (aS.Count() > 0 /*&& (DateTime.Now.Hour == 23 || DateTime.Now.Hour == 11)*/) //as 23 ou 11 horas de cada dia envia um email
                    {
                        List<int> index = PositionInAlertSystemToSendEmail();
                        index = index.OrderByDescending(i => i).ToList();

                        SendEmail(index);

                        for (var i = 0; i < index.Count(); i++)
                        {
                            aS.Remove(aS[index[i]]);
                        }
                    }
                    if (allDisable)
                    {
                        Thread.Sleep(7200000);//2hours
                    }
                    else
                    {
                        if (rainForecast)
                        {
                            Thread.Sleep(120000);//2minutes
                        }
                        else
                        {
                            Thread.Sleep(300000);//5minutes
                        }
                    }
                    allDisable = true;
                    rainForecast = false;
                }
            }
        }

        private void createExcelFile(IEnumerable<IGrouping<int, leituras>> allleituras)
        {

            string path = $"C:\\SmartIrrigation\\RegistosDiarios.csv";

            DataTable table = new DataTable();
            table.Columns.Add("Id", typeof(int));
            table.Columns.Add("IdArduino", typeof(int));
            table.Columns.Add("Humidade", typeof(string));
            table.Columns.Add("Tenperatura", typeof(string));
            table.Columns.Add("PH", typeof(string));
            table.Columns.Add("Caudal", typeof(string));
            table.Columns.Add("Parque", typeof(string));
            table.Columns.Add("Zona", typeof(string));
            table.Columns.Add("Distrito", typeof(string));
            table.Columns.Add("DataLeitura", typeof(DateTime));
            table.Columns.Add("is_Enable", typeof(int));


            foreach (var sensor in allleituras)
            {
                foreach (var leitura in sensor)
                {
                    //Para evitar estar sempre a escrever dados duplicados, verificamos se o ficheiro ja existe, se não existir cria o ficheiro com todas
                    //as informações da BD, se já existir apenas escreve os dados cujas leituras foram feitas no dia de hoje
                    if (File.Exists(path))
                    {
                        if (leitura.Data.Date == DateTime.Now.Date)
                        {
                            table.Rows.Add(leitura, leitura.Id_arduino, leitura.Sensor_humidade,
                                leitura.Sensor_temperatura, leitura.Sensor_ph,
                                leitura.Caudal, leitura.Parque, leitura.Zona, leitura.Distrito, leitura.Data, leitura.Is_enable);
                        }

                    }
                    else
                    {
                        table.Rows.Add(leitura, leitura.Id_arduino, leitura.Sensor_humidade,
                            leitura.Sensor_temperatura, leitura.Sensor_ph,
                            leitura.Caudal, leitura.Parque, leitura.Zona, leitura.Distrito, leitura.Data, leitura.Is_enable);
                    }

                }

            }

            if (File.Exists(path))
            {
                table.ToCsv(path, true);
            }
            else
            {
                table.ToCsv(path, false);
            }


        }

        private void GetAPICurrentConditions()
        {
            string urlLocation = "http://api.openweathermap.org/data/2.5/uvi?appid=f89ae8cee2ecd42dd496cbbae5897b71&lat=41.55&lon=-8.42";
            var client = new WebClient();
            string content = client.DownloadString(urlLocation);
            var UV = JsonConvert.DeserializeObject<UV>(content);

            string urlCurrentConditions = "http://api.openweathermap.org/data/2.5/weather?q=Braga&appid=f89ae8cee2ecd42dd496cbbae5897b71";
            var client1 = new WebClient();
            string content1 = client1.DownloadString(urlCurrentConditions);
            Conditions currentConditions = JsonConvert.DeserializeObject<Conditions>(content1);

            string urlLocation1 = "http://dataservice.accuweather.com/locations/v1/cities/search?apikey=YYNuNGQLhYlBuVZw4iTXaBKJzGJbCJ5T&q=Braga%2CPortugal";
            var client2 = new WebClient();
            string content2 = client2.DownloadString(urlLocation1);
            var locationKey = JsonConvert.DeserializeObject<List<location>>(content2);

            string urlForecast = "http://dataservice.accuweather.com/forecasts/v1/daily/5day/" + locationKey[0].Key.ToString() + "?apikey=%09YYNuNGQLhYlBuVZw4iTXaBKJzGJbCJ5T";
            var client3 = new WebClient();
            string content3 = client3.DownloadString(urlForecast);
            content3 = content3.Substring(content3.IndexOf('['));
            content3 = content3.Remove(content3.Length - 1);
            var forecast5days = JsonConvert.DeserializeObject<List<forecast>>(content3);

            string rain = forecast5days[1].Day.IconPhrase.ToString() == "Rain" || forecast5days[1].Day.IconPhrase.ToString() == "Showers" || forecast5days[1].Night.IconPhrase.ToString() == "Showers" || forecast5days[1].Night.IconPhrase.ToString() == "Rain" ? 1.ToString() : 0.ToString();
            var activate = false;

            if (Convert.ToInt32(currentConditions.Main.Humidity) < 35)
            {
                if (rain == "0") //Se não chover
                {
                    if (Convert.ToInt32((currentConditions.Main.Temp - 273)) > 14 && Convert.ToInt32((currentConditions.Main.Temp - 273)) < 26)
                    {
                        if (DateTime.Now.Hour < 7 || DateTime.Now.Hour > 21)
                        {
                            activate = true;
                        }
                    }
                }
            }

            var isEnable = activate ? 1.ToString() : 0.ToString();

            mConn = new MySqlConnection("server=db4free.net;Port=3306;old guids=true;User ID=basededadosteste;password=12345678;database = basededadosteste;");
            mConn.Open();

            if (mConn.State == ConnectionState.Open)
            {
                string query = "INSERT INTO `leituras`(`humidade`, `temperatura`, `id_arduino`, `ph`, `caudal`, `uv`, `previsao`, `isEnable`) VALUES (" + currentConditions.Main.Humidity.ToString() + ", " + (currentConditions.Main.Temp - 273).ToString().Replace(',', '.') + ", 1, 5, 15, " + UV.value.ToString().Replace(',', '.') + ", " + rain + ", " + isEnable + ")";
                MySqlCommand command = new MySqlCommand(query, mConn);
                command.ExecuteNonQuery();
            }
            mConn.Close();
        }

    }
}